package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.SousCategory;
import com.example.demo.entity.SubCategory;
@Repository
public interface SousCategoryRepository extends CrudRepository<SousCategory,Integer>{

}
