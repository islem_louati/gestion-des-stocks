package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

	Optional<Product> findByproductCde(String productCde);

	Product getByproductCde(String productCde);

	Iterable<Product> getByBrand(String brand);

	Iterable<Product> getBynameProduct(String nameProduct);

	@Query("select  c FROM Product c where c.souscategorys.namesouscategory=:namesouscategory")
	Iterable<Product> getByCategrys(@Param("namesouscategory") String namesouscategory);

	@Query("select  c FROM SousCategory c where c.subcategory.namecategory=:namecategory")
	Iterable<Product> getBySubCategrys(@Param("namecategory") String namecategory);

	@Query("select  c FROM SousCategory c where c.namesouscategory=:namesouscategory AND c.subcategory.namecategory=:namecategory")
	Iterable<Product> getByCategrysAndsousCategory(@Param("namesouscategory") String namesouscategory,
			@Param("namecategory") String namecategory);

	@Query("select  c FROM Product c where c.nameProduct=:nameProduct AND c.souscategorys.namesouscategory=:namesouscategory")
	Iterable<Product> getBynameProductAndCategory(@Param("nameProduct") String nameProduct,
			@Param("namesouscategory") String namesouscategory);

	@Query("select  c FROM Product c where c.nameProduct=:nameProduct AND c.souscategorys.namesouscategory=:namesouscategory AND c.souscategorys.subcategory.namecategory=:namecategory")
	Iterable<Product> getBynameProductAndCategoryAndSousCategory(@Param("nameProduct") String nameProduct,
			@Param("namesouscategory") String namesouscategory, @Param("namecategory") String namecategory);

}
