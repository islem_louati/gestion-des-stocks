package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.SousCategory;
import com.example.demo.service.SousCategoryService;

@RestController
public class SousCategoryController {
	@Autowired
	SousCategoryService souscateservice;

	@PostMapping("/addSousCategory")
	@ResponseBody
	public SousCategory addCategory(@RequestBody SousCategory category) {
		souscateservice.addSousCategory(category);
		return category;
	}

	/*
	 * http://localhost:8095/SpringMVC/getallCategory
	 */
	@GetMapping("/getallSousCategory")
	@ResponseBody
	public List<SousCategory> getsousCategory() {
		return souscateservice.getAllCategories();
	}
}
