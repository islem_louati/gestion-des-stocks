package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.entity.ProductDTO;
import com.example.demo.entity.ProductDTO2;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	ProductService prodserv;
	@Autowired
	ProductRepository productrep;

	@PostMapping("/addProduct")
	@ResponseBody
	public ResponseEntity<String> addProduct(@RequestBody Product product) {
		boolean productExists = productrep.findByproductCde(product.getProductCde()).isPresent();
		String message = "";

		if (productExists) {
			message = "barcode exist " + product.getNameProduct();
			return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
		}
		if ((!(product.getProductCde().chars().allMatch(Character::isLetterOrDigit))) ||( product.getProductCde().length()<10 || product.getProductCde().length()>15 )) {
			return new ResponseEntity<>("code a bar doit etre alphanumérique entre 10 et 15 caractère", HttpStatus.BAD_REQUEST);
		} else {
			prodserv.addProduct(product);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		}
	}

	@GetMapping("/getallProduct")
	@ResponseBody
	public List<ProductDTO> getAllProduct() {
		return prodserv.getallProduct();
	}

	@GetMapping("/getallProductwithsaleTarif")
	@ResponseBody
	public List<ProductDTO2> getAllProductwithsaletarif() {
		return prodserv.getallProductwithSaleTarif();
	}

	@GetMapping("/getallProductByBarCode/{productCde}")
	@ResponseBody
	public Product getallProductByBarCode(@PathVariable("productCde") String productCde) {
		return prodserv.getProductByBarCode(productCde);
	}

	@GetMapping("/getallProductBySousCategrys/{namesouscategory}")
	@ResponseBody
	public Iterable<Product> getallProductBySousCategrys(@PathVariable("namesouscategory") String namesouscategory) {
		return prodserv.getProductByCategory(namesouscategory);
	}

	@GetMapping("/getallProductByName/{nameProduct}")
	@ResponseBody
	public Iterable<Product> getallProductByName(@PathVariable("nameProduct") String nameProduct) {
		return prodserv.getProductByname(nameProduct);
	}

	@GetMapping("/getallProductByBrand/{brand}")
	@ResponseBody
	public Iterable<Product> getallProductByBrand(@PathVariable("brand") String brand) {
		return prodserv.getProductByBrand(brand);
	}

	@GetMapping("/getallProductBySubCtegry/{namecategory}")
	@ResponseBody
	public Iterable<Product> getallProductBySubCtegry(@PathVariable("namecategory") String namecategory) {
		return prodserv.getProductBySubCategory(namecategory);
	}

	@GetMapping("/getallProductByCategryAndSusCategry/{namecategory}/{namesouscategory}")
	@ResponseBody
	public Iterable<Product> getallProductByCategryAndSusCategry(@PathVariable("namecategory") String namecategory,
			@PathVariable("namesouscategory") String namesouscategory) {
		return prodserv.getProductByCategoryAndSubCateg(namecategory, namesouscategory);
	}

	@GetMapping("/getallProductByNamePrductAndSusCategry/{namesouscategory}/{nameProduct}")
	@ResponseBody
	public Iterable<Product> getallProductByNamePrductAndSusCategry(
			@PathVariable("namesouscategory") String namesouscategory,
			@PathVariable("nameProduct") String nameProduct) {
		return prodserv.getProductByNameAndSuscategry(namesouscategory, nameProduct);
	}

	@GetMapping("/getallProductByNamePrductAndSubCategAndSusCategry/{namecategory}/{namesouscategory}/{nameProduct}")
	@ResponseBody
	public Iterable<Product> getallProductByNamePrductAndSubCategAndSusCategry(
			@PathVariable("namecategory") String namecategory,
			@PathVariable("namesouscategory") String namesouscategory,
			@PathVariable("nameProduct") String nameProduct) {
		return prodserv.getProductByNameAndCategryAndSubCategry(namecategory, namesouscategory, nameProduct);
	}

	@DeleteMapping("/deleteProductByBarCode/{productCde}")
	@ResponseBody
	public void deleteCategoryById(@PathVariable("productCde") String productCde) {

		prodserv.deletebybarcoode(productCde);

	}

}
