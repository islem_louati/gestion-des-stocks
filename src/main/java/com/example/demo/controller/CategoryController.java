package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.SubCategory;
import com.example.demo.service.CategoryService;

@RestController
public class CategoryController {
	@Autowired
	CategoryService catservice;

	@PostMapping("/addCategory")
	@ResponseBody
	public SubCategory addCategory(@RequestBody SubCategory category) {
		catservice.addCategory(category);
		return category;
	}

	/*
	 * http://localhost:8095/SpringMVC/getallCategory
	 */
	@GetMapping("/getallCategory")
	@ResponseBody
	public List<SubCategory> getCategoryById() {
		return catservice.getAllCategories();
	}
}
