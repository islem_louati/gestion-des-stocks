package com.example.demo.entity;

public class ProductDTO {

	private int productId;

	private String nameProduct;

	private int quantity;

	private float purchaseTariff;

	private float som = purchaseTariff * quantity;

	public float getSom() {
		return som;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public ProductDTO(int productId, String nameProduct, int quantity) {
		super();
		this.productId = productId;
		this.nameProduct = nameProduct;
		this.quantity = quantity;
	}

	public ProductDTO(int productId, String nameProduct, int quantity, float som) {
		super();
		this.productId = productId;
		this.nameProduct = nameProduct;
		this.quantity = quantity;
		this.som = som;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPurchaseTariff() {
		return purchaseTariff;
	}

	public void setPurchaseTariff(float purchaseTariff) {
		this.purchaseTariff = purchaseTariff;
	}

}
