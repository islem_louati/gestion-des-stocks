package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;

	@Column(name = "name")
	private String nameProduct;

	@Column(name = "product_Code")
	@Size(min = 10, max = 15,message = "Bar Code should between 10 and 15 ")
	private String productCde;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "purchase_tariff")
	private float purchaseTariff;

	@Column(name = "sale_tariff")
	private float saleTariff;

	@Column(name = "Brand")
	private String brand;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "souscategorys")
	private SousCategory souscategorys;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public String getProductCde() {
		return productCde;
	}

	public void setProductCde(String productCde) {
		this.productCde = productCde;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPurchaseTariff() {
		return purchaseTariff;
	}

	public void setPurchaseTariff(float purchaseTariff) {
		this.purchaseTariff = purchaseTariff;
	}

	public float getSaleTariff() {
		return saleTariff;
	}

	public void setSaleTariff(float saleTariff) {
		this.saleTariff = saleTariff;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public SousCategory getSouscategorys() {
		return souscategorys;
	}

	public void setSouscategorys(SousCategory souscategorys) {
		this.souscategorys = souscategorys;
	}

	public Product(String nameProduct) {
		super();
		this.nameProduct = nameProduct;
	}

	public Product(String nameProduct, int quantity, float purchaseTariff, float saleTariff) {
		super();
		this.nameProduct = nameProduct;
		this.quantity = quantity;
		this.purchaseTariff = purchaseTariff;
		this.saleTariff = saleTariff;
	}

	public Product(int productId, String nameProduct, String productCde, int quantity, float purchaseTariff,
			float saleTariff, String brand, SousCategory souscategorys) {
		super();
		this.productId = productId;
		this.nameProduct = nameProduct;
		this.productCde = productCde;
		this.quantity = quantity;
		this.purchaseTariff = purchaseTariff;
		this.saleTariff = saleTariff;
		this.brand = brand;
		this.souscategorys = souscategorys;
	}

	public Product() {
		super();
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", nameProduct=" + nameProduct + ", productCde=" + productCde
				+ ", quantity=" + quantity + ", purchaseTariff=" + purchaseTariff + ", saleTariff=" + saleTariff
				+ ", brand=" + brand + ", souscategorys=" + souscategorys + "]";
	}

}
