package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class SubCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int categoryId;

	@Column(name = "name_category")
	private String namecategory;

	@OneToMany(mappedBy = "subcategory", cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, fetch = FetchType.EAGER)
	private List<SousCategory> souscategorys;

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getNamecategory() {
		return namecategory;
	}

	public void setNamecategory(String namecategory) {
		this.namecategory = namecategory;
	}

	public List<SousCategory> getSouscategorys() {
		return souscategorys;
	}

	public void setSouscategorys(List<SousCategory> souscategorys) {
		this.souscategorys = souscategorys;
	}

	public SubCategory(int categoryId, String namecategory, List<SousCategory> souscategorys) {
		super();
		this.categoryId = categoryId;
		this.namecategory = namecategory;
		this.souscategorys = souscategorys;
	}

	public SubCategory() {
		super();
	}

	@Override
	public String toString() {
		return "SubCategory [categoryId=" + categoryId + ", namecategory=" + namecategory + ", souscategorys="
				+ souscategorys + "]";
	}

}
