package com.example.demo.entity;

public class ProductDTO2 {

	private String nameProduct;

	private float saleTariff;

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public float getSaleTariff() {
		return saleTariff;
	}

	public void setSaleTariff(float saleTariff) {
		this.saleTariff = saleTariff;
	}

	public ProductDTO2(String nameProduct, float saleTariff) {
		super();
		this.nameProduct = nameProduct;
		this.saleTariff = saleTariff;
	}

}
