package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class SousCategory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int souscategryId;

	@Column(name = "name_sous_category")
	private String namesouscategory;

	@OneToMany(mappedBy = "souscategorys", cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE }, fetch = FetchType.EAGER)
	private List<Product> products;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "subcategorys")
	private SubCategory subcategory;

	public int getSouscategryId() {
		return souscategryId;
	}

	public void setSouscategryId(int souscategryId) {
		this.souscategryId = souscategryId;
	}

	public String getNamesouscategory() {
		return namesouscategory;
	}

	public void setNamesouscategory(String namesouscategory) {
		this.namesouscategory = namesouscategory;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public SubCategory getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(SubCategory subcategory) {
		this.subcategory = subcategory;
	}

	public SousCategory(int souscategryId, String namesouscategory, List<Product> products, SubCategory subcategory) {
		super();
		this.souscategryId = souscategryId;
		this.namesouscategory = namesouscategory;
		this.products = products;
		this.subcategory = subcategory;
	}

	public SousCategory() {
		super();
	}

	@Override
	public String toString() {
		return "SousCategory [souscategryId=" + souscategryId + ", namesouscategory=" + namesouscategory + ", products="
				+ products + ", subcategory=" + subcategory + "]";
	}

}
