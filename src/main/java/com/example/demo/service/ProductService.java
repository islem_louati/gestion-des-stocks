package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Product;
import com.example.demo.entity.ProductDTO;
import com.example.demo.entity.ProductDTO2;

public interface ProductService {
	void addProduct(Product product);

	List<ProductDTO> getallProduct();

	List<ProductDTO2> getallProductwithSaleTarif();

	void deletebybarcoode(String productCde);

	Product getProductByBarCode(String productCde);

	Iterable<Product> getProductByBrand(String brand);

	Iterable<Product> getProductByname(String nameProduct);

	Iterable<Product> getProductByCategory(String namesouscategory);

	Iterable<Product> getProductBySubCategory(String namecategory);

	Iterable<Product> getProductByCategoryAndSubCateg(String namesouscategory, String namecategory);

	Iterable<Product> getProductByNameAndSuscategry(String nameProduct, String namesouscategory);

	Iterable<Product> getProductByNameAndCategryAndSubCategry(String nameProduct, String namesouscategory,
			String namecategory);

}
