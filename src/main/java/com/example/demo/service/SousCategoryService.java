package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.SousCategory;

public interface SousCategoryService {

	void addSousCategory(SousCategory category);

	List<SousCategory> getAllCategories();

}
