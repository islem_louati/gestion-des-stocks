package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.entity.ProductDTO;
import com.example.demo.entity.ProductDTO2;
import com.example.demo.repository.ProductRepository;

@Service
public class ProductServiceImp implements ProductService {

	@Autowired
	ProductRepository prodrep;

	@Override
	public void addProduct(Product product) {

		prodrep.save(product);
	}

	@Override
	public List<ProductDTO> getallProduct() {

		List<ProductDTO> products = new ArrayList<>();
		Product p;
		for (Product pp : prodrep.findAll()) {
			float som = pp.getPurchaseTariff() * pp.getQuantity();
			products.add(new ProductDTO(pp.getProductId(), pp.getNameProduct(), pp.getQuantity(), som));
		}

		return products;

	}

	@Override
	public List<ProductDTO2> getallProductwithSaleTarif() {

		List<ProductDTO2> products = new ArrayList<>();
		Product p;
		for (Product pp : prodrep.findAll()) {
			float som = pp.getPurchaseTariff() * pp.getQuantity();
			products.add(new ProductDTO2(pp.getNameProduct(), pp.getSaleTariff()));
		}

		return products;

	}

	@Override
	public void deletebybarcoode(String productCde) {
		Product p = prodrep.getByproductCde(productCde);
		prodrep.delete(p);
	}

	@Override
	public Product getProductByBarCode(String productCde) {
		return prodrep.getByproductCde(productCde);
	}

	@Override
	public Iterable<Product> getProductByBrand(String brand) {
		return prodrep.getByBrand(brand);
	}

	@Override
	public Iterable<Product> getProductByname(String nameProduct) {
		return prodrep.getBynameProduct(nameProduct);
	}

	@Override
	public Iterable<Product> getProductByCategory(String namesouscategory) {
		return prodrep.getByCategrys(namesouscategory);
	}

	@Override
	public Iterable<Product> getProductBySubCategory(String namecategory) {
		return prodrep.getBySubCategrys(namecategory);
	}

	@Override
	public Iterable<Product> getProductByCategoryAndSubCateg(String namesouscategory, String namecategory) {
		return prodrep.getByCategrysAndsousCategory(namesouscategory, namecategory);
	}

	@Override
	public Iterable<Product> getProductByNameAndSuscategry(String nameProduct, String namesouscategory) {
		return prodrep.getBynameProductAndCategory(nameProduct, namesouscategory);
	}

	@Override
	public Iterable<Product> getProductByNameAndCategryAndSubCategry(String nameProduct, String namesouscategory,
			String namecategory) {
		return prodrep.getBynameProductAndCategoryAndSousCategory(nameProduct, namesouscategory, namecategory);
	}

}
