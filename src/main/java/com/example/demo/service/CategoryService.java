package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.SubCategory;

public interface CategoryService {

	void addCategory(SubCategory category);

	List<SubCategory> getAllCategories();

}
