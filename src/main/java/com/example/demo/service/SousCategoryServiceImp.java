package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.SousCategory;
import com.example.demo.repository.SousCategoryRepository;

@Service
public class SousCategoryServiceImp implements SousCategoryService {
	@Autowired
	SousCategoryRepository souscatrep;

	@Override
	public void addSousCategory(SousCategory category) {
		souscatrep.save(category);
	}

	@Override
	public List<SousCategory> getAllCategories() {
		return (List<SousCategory>) souscatrep.findAll();
	}

}
