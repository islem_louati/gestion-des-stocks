package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.SubCategory;
import com.example.demo.repository.CategoryRepository;

@Service
public class CategoryServiceimp implements CategoryService {
	@Autowired
	CategoryRepository cartrep;

	@Override
	public void addCategory(SubCategory category) {
		cartrep.save(category);

	}

	@Override
	public List<SubCategory> getAllCategories() {
		return (List<SubCategory>) cartrep.findAll();
	}

}
